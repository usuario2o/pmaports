# Maintainer: Bart Ribbers <bribbers@disroot.org>
# Contributor: Bart Ribbers <bribbers@disroot.org>
pkgname=kactivities-stats
pkgver=5.55.0
pkgrel=0
arch="all"
pkgdesc="A library for accessing the usage data collected by the activities system"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends_dev="boost-dev qt5-qtbase-dev kconfig-dev kactivities-dev graphviz-dev qt5-qttools-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
builddir="$srcdir/$pkgname-$pkgver/build"

prepare() {
	mkdir "$builddir"
}

build() {
	cd "$builddir"

	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="09f90120f16738e2aacfcc597ea3c4594451b5033b6f6fe2f6c10266ab9fc199ae062680aa1a250e75caa85a65e21a7c32283410081589b50aa6d13ca9fa2f13  kactivities-stats-5.55.0.tar.xz"
