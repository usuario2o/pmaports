# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kglobalaccel
pkgver=5.55.0
pkgrel=0
pkgdesc="Add support for global workspace shortcuts"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends_dev="kconfig-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kwindowsystem-dev qt5-qtx11extras-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev xcb-util-keysyms-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring running X11

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="06593bb4839196545d9d5104e43a62082a1b0dec1d8c6a5280fb013443660e79782a54c188916b220c8610541c367f69b6814e2b8894a5210f6a2d65891447e8  kglobalaccel-5.55.0.tar.xz"
